#!/bin/bash

#
#create 3 pbs scripts to run a WRF job
#including getting the boundary data and sending the output data
# and calling the next job
#
# Copy-paste the namelist.input you have used on CCRC machines to run real between
# the lines "cat >! ./namelist.input << EOF_namelist" and "EOF_namelist"

# Go to WRF run directory
cd ${COUPLED_PATH}

cat > ./getbdy_${SYEAR}_${SMONTH}.pbs << EOF_getbdy
#!/bin/csh
#PBS -m ae
#PBS -M ${EMAIL}
#PBS -P ${PROJECT}
#PBS -q normal
#PBS -l walltime=00:20:00
#PBS -l mem=4GB
#PBS -l ncpus=1
#PBS -j oe
#PBS -l wd
#PBS -l storage=gdata/${PROJECT}+scratch/${PROJECT}

module purge
module load pbs
module load nco/4.7.7

# Go to WRF run directory
cd ${COUPLED_PATH}

# Get LIS restart file and lis.config
if (${COUPLED}) then

    rm -f LIS*.*rst
    ln -sf ${LISOUT_PATH}/LIS_RST_${LSM}_${SYEAR}${SMONTH}${SDAY}0000.d01.nc .
    set es=\$?
    [ \$es -ne 0 ] && exit \$es

    rm -f lis_input.d01.nc
    ln -sf ${LIS_PATH}/lis_input.d01.nc .

    rm -f lis.config
    ln -sf ${DECK_PATH}/${LIS_CONFIG} lis.config
    set es=\$?
    [ \$es -ne 0 ] && exit \$es

    # Get MODEL_OUTPUT file that define the output for LIS
    ln -sf ${DECK_PATH}/${MODEL_OUTPUT_AR} .
    set es=\$?
    [ \$es -ne 0 ] && exit \$es

    # Get python code for post-processing
    ln -sf ${DECK_PATH}/post-process.py .
    set es=\$?
    [ \$es -ne 0 ] && exit \$es

endif

rm wrfrst_d0*
rm wrfbdy*
rm wrfinput*
rm wrflowinp*

#get files. See if concatenation is needed or not.
if (${WPS_monthly_files}) then
  set cm=${smonth}
  set cy=${SYEAR}
  set nm=${WRF_nmonths}
  set i=1
  cp ${BDYDATA_PATH}/wrfbdy_d01_${SYEAR}-${SMONTH} wrfbdy_d01
  cp ${BDYDATA_PATH}/wrflowinp_d01_${SYEAR}-${SMONTH} wrflowinp_d01

  while ( \$i < ${WRF_nmonths} )
     @ cm = \$cm + 1
     if ( \$cm > 12 ) then
       set cm=1
       @ cy = \$cy + 1
     endif
     set date=\`printf %02i \$cm\`

     ncrcat -A wrfbdy_d01 ${BDYDATA_PATH}/wrfbdy_d01_\$cy-\${date} wrfbdy_d01
     ncrcat -A wrflowinp_d01 ${BDYDATA_PATH}/wrflowinp_d01_\$cy-\${date} wrflowinp_d01

     @ i = \$i + 1
   end
else
   ln -sf ${BDYDATA_PATH}/wrfbdy_d01_${SYEAR}-${SMONTH} wrfbdy_d01
   ln -sf ${BDYDATA_PATH}/wrflowinp_d01_${SYEAR}-${SMONTH} wrflowinp_d01
endif

if (${FIRST}) then
  ln -sf ${BDYDATA_PATH}/wrfinput_d01_${SYEAR}-${SMONTH} wrfinput_d01
  set es=\$?
  [ \$es -ne 0 ] && exit \$es
endif
if (${NOFIRST}) then
  scp -p ${WRFOUT_PATH}/wrfrst_d??_${SYEAR}-${SMONTH}* .
  set es=\$?
  [ \$es -ne 0 ] && exit \$es
endif

EOF_getbdy

cat > ./C${SYEAR}${SMONTH}${SDAY}.pbs << EOF_wrf
#!/bin/csh
#PBS -m ae
#PBS -M ${EMAIL}
#PBS -P ${PROJECT}
#PBS -q normal
#PBS -l walltime=15:00:00
#PBS -l mem=2400GB
#PBS -l ncpus=624
#PBS -j oe
#PBS -l wd
#PBS -l storage=gdata/hh5+gdata/${PROJECT}+scratch/${PROJECT}+gdata/w35

module purge
module load pbs
module load openmpi/4.0.2

# Go to WRF run directory
cd ${COUPLED_PATH}

rm rsl.*

#write the namelist.input
cat >! ./namelist.input << EOF_namelist
&time_control
 run_days                            = ${RUNDAYS},
 run_hours                           = 0,
 run_minutes                         = 0,
 run_seconds                         = 0,
 start_year                          = ${SYEAR}, ${SYEAR}, ${SYEAR},
 start_month                         = ${SMONTH},   ${SMONTH},   ${SMONTH},
 start_day                           = ${SDAY},   ${SDAY},   ${SDAY},
 start_hour                          = 00,   00,   00,
 start_minute                        = 00,   00,   00,
 start_second                        = 00,   00,   00,
 end_year                            = ${NYEAR}, ${NYEAR}, ${NYEAR},
 end_month                           = ${NMONTH},   ${NMONTH},   ${NMONTH},
 end_day                             = ${NDAY},   ${NDAY},   ${NDAY},
 end_hour                            = 00,   00,   12,
 end_minute                          = 0,  ${OLAP},  ${OLAP},
 end_second                          = 00,   00,   00,
 interval_seconds                    = 21600
 input_from_file                     = .true.,.true.,.false.,
 history_interval                    = 180, 180,   60,
 frames_per_outfile                  = 1000, 36, 1000,
 restart                             = ${ISRESTART}
 restart_interval                    = ${RES_WRF}
 override_restart_timers             = .true.
 io_form_history                     = 2
 io_form_restart                     = 2
 io_form_input                       = 2
 io_form_boundary                    = 2
 debug_level                         = 0
 auxinput4_inname                    = "wrflowinp_d<domain>"
 auxinput4_interval                  = 360,360,
 io_form_auxinput4                   = 2
 output_diagnostics                  = 1
 auxhist3_outname                    = "wrfxtrm_d<domain>_<date>"
 io_form_auxhist3                    = 2
 auxhist3_interval                   = 1440,1440,1440,
 frames_per_auxhist3                 = 1,1,1,
 iofields_filename                   = "my_file_d01.txt"
 /

&domains
 time_step                           = ${WRF_ts},
 time_step_fract_num                 = 0,
 time_step_fract_den                 = 1,
 max_dom                             = ${max_dom},
 s_we                                = 1,     1,     1,
 e_we                                = ${E_WE_AR}
 s_sn                                = 1,     1,     1,
 e_sn                                = ${E_SN_AR}
 s_vert                              = 1,     1,     1,
 e_vert                              = 30,    30,    28,
 eta_levels = 1.00,0.995,0.99,0.98,0.97,0.96,0.94,0.92,0.89,0.86,0.83,0.80,0.77,0.72,0.67,0.62,0.57,0.52,0.47,0.42,0.37,0.32,0.27,0.22,0.17,0.12,0.07,0.04,0.02,0.00
 num_metgrid_levels                  = 38
 num_metgrid_soil_levels             = 4
 dx                                  = ${DX_AR}
 dy                                  = ${DY_AR}
 grid_id                             = ${GRID_ID_AR}
 parent_id                           = ${PARENT_ID_AR}
 i_parent_start                      = ${I_PARENT_START_AR}
 j_parent_start                      = ${J_PARENT_START_AR}
 parent_grid_ratio                   = ${PARENT_GRID_RATIO_AR}
 parent_time_step_ratio              = 1,     5,      3,
 feedback                            = 0,
 smooth_option                       = 0
 /

&lis
 lis_landcover_type = 2,
 lis_filename='lis4real_input.d01.nc'
/

&physics
 mp_physics                          = 4,     4,     3,
 mp_zero_out                         = 2
 mp_zero_out_thresh                  = 1.e-8
 ra_lw_physics                       = 4,     4,     4,
 ra_sw_physics                       = 4,     4,     4,
 radt                                = 10,    10,    10,
 cam_abs_freq_s                      = 10800
 levsiz                              = 59
 paerlev                             = 29
 cam_abs_dim1                        = 4
 cam_abs_dim2                        = 28
 sf_sfclay_physics                   = 2,     2,     2,
 sf_surface_physics                  = 55,     5,     1,
 bl_pbl_physics                      = 2,     2,     2,
 bldt                                = 0,     0,     0,
 cu_physics                          = 16,     16,     16,
 cudt                                = 5,     0,     5,
 isfflx                              = 1,
 ifsnow                              = 0,
 icloud                              = 1,
 surface_input_source                = 1,
 num_soil_layers                     = 6,
 num_land_cat                        = 20,
 sf_urban_physics                    = 0,
 sst_update                          = 1,
 sst_skin                            = 1,
 tmn_update                          = 1,
 lagday                              = 150,
 usemonalb                           = .false.
 slope_rad                           = 1,
 maxiens                             = 1,
 maxens                              = 3,
 maxens2                             = 3,
 maxens3                             = 16,
 ensdim                              = 144,
 bucket_mm                           = 1000000.,
 /

 &fdda
 /

&dynamics
 rk_ord                              = 3,
 w_damping                           = 0,
 diff_opt                            = 1,
 km_opt                              = 4,
 diff_6th_opt                        = 0,
 diff_6th_factor                     = 0.12,
 base_temp                           = 290.
 damp_opt                            = 3,
 zdamp                               = 5000.,  5000.,  5000.,
 dampcoef                            = 0.05,   0.05,   0.01
 khdif                               = 0,      0,      0,
 kvdif                               = 0,      0,      0,
 non_hydrostatic                     = .true., .true., .true.,
 moist_adv_opt                       = 1, 1, 1,
 scalar_adv_opt                      = 1, 1, 1,
 /

&bdy_control
 spec_bdy_width                      = 10,
 spec_zone                           = 1,
 relax_zone                          = 9,
 specified                           = .true., .false.,.false.,
 nested                              = .false., .true., .true.,
 /

 &grib2
 /

 &namelist_quilt
 nio_tasks_per_group = 0,
 nio_groups = 1,
 /
EOF_namelist

rm wrfout_d0*

limit stacksize unlimit

mpirun -mca mpi_paffinity_alone 1 -np \$PBS_NCPUS ${CODEDIR_ROOT}/WRFV3/main/wrf.exe

EOF_wrf


cat >./putout_${SYEAR}_${SMONTH}.pbs << EOF_putout
#!/bin/csh
#PBS -m ae
#PBS -M ${EMAIL}
#PBS -P ${PROJECT}
#PBS -q normalbw
#PBS -l walltime=05:00:00
#PBS -l mem=10GB
#PBS -l ncpus=8
#PBS -j oe
#PBS -l wd
#PBS -l storage=gdata/hh5+gdata/${PROJECT}+scratch/${PROJECT}
#PBS -l jobfs=10GB

module purge
module load pbs
module load nco/4.7.7 
module use /g/data3/hh5/public/modules
module load conda/analysis3

# Go to WRF run directory
cd ${COUPLED_PATH}

set tempdir = "wrftemp"

# remove previous wrftemp directory if exists
if (-e \$tempdir) then
   rm -r \$tempdir
endif

# create empty wrftemp directory
mkdir \$tempdir
echo Move wrf outputs to post-processing directory
mv ./wrf{o,r,d,h,x,2}* \$tempdir/.

# Save LIS outputs before processing
if ( ${COUPLED} ) then
  echo Copy LIS outputs to post-processing directory
  # remove previous OUTPUT_postprocess directory if exists
  if (-e OUTPUT_postprocess) then
    rm -r OUTPUT_postprocess
  endif
 
  mv OUTPUT OUTPUT_postprocess
  # Remove last restart LIS file if any
  rm LIS_RST*

endif


### Start post-processing
# The idea is to create big files to save on /g/data. If you want it differently please update as you like.
cd \$tempdir

#wrfdly
# echo Concatenate wrfdly
# set nfiles=\`ls -1 wrfdly* | wc -l\`
# if ( \$nfiles != 0 ) then
#    set listfiles=( \`ls -1 wrfdly*\` )
#    ncrcat -O \$listfiles tmp.nc
#    rm -f \$listfiles
#    mv tmp.nc \${listfiles[1]}
# endif

#wrfxtrm
echo Concatenate wrfxtrm
set nfiles=\`ls -1 wrfxtrm* | wc -l\`
if ( \$nfiles != 0 ) then
   set listfiles=( \`ls -1 wrfxtrm*\` )
   ncrcat -O \$listfiles tmp.nc
   rm -f \$listfiles
   mv tmp.nc \${listfiles[1]}
endif

cd ../    # Back to run directory


##

echo Transfer
#rsync files - if any of them fail then fail out of the pbs job
rsync -vrlt ./\$tempdir/wrfout_d* ${WRFOUT_PATH}/.
set es=\$?
[ \$es -ne 0 ] && exit \$es
rsync -vrlt ./\$tempdir/wrfrst_* ${WRFOUT_PATH}/.
set es=\$?
[ \$es -ne 0 ] && exit \$es
rsync -vrlt ./\$tempdir/wrfxtrm_d??_* ${WRFOUT_PATH}/.
rsync -vrlt ./\$tempdir/wrfdly_d??_* ${WRFOUT_PATH}/.

# Clean up files
rm -f \$tempdir/*


if ( ${COUPLED} ) then
    cd OUTPUT_postprocess
    echo In OUTPUT_postprocess:
    ls
    echo Post-processing LIS outputs

    # Stats file
    set dom=1
    while ( \$dom <= ${max_dom} )
        mv SURFACEMODEL.d0\${dom}.stats ${LSM}stats.d0\${dom}.stats.${SYEAR}${SMONTH}
        @ dom++
    end

    # Call python code for netcdf outputs
    python ${COUPLED_PATH}/post-process.py -o ${LISOUT_PATH}/. -s ${SYEAR}-${SMONTH} -e ${NYEAR}-${NMONTH} ${COUPLED_PATH}/OUTPUT_postprocess/SURFACEMODEL
    set es=\$?
    [ \$es -ne 0 ] && exit \$es

    # Copy outputs from LIS

    # Move all restart file to this level.
    mv SURFACEMODEL/LIS_RST* .

    # Remove the directory structure before copy
    rm -r SURFACEMODEL

    cd ../
    rsync -vrlt OUTPUT_postprocess/* ${LISOUT_PATH}/.

    # Catch error
    set es=\$?
    [ \$es -ne 0 ] && exit \$es

    # Clean up
    echo Clean up
    cd ../
    echo $pwd
    rm -rf OUTPUT_postprocess

endif

# Get next script: We start next script before post-processing to save time.
ln -sf ${DECK_PATH}/runwrf_${NYEAR}_${NMONTH}_${NDAY}.deck .
set es=\$?
[ \$es -ne 0 ] && exit \$es

./runwrf_${NYEAR}_${NMONTH}_${NDAY}.deck


EOF_putout

getjobid=`qsub getbdy_${SYEAR}_${SMONTH}.pbs`
wrfjobid=`qsub -W depend=afterok:$getjobid C${SYEAR}${SMONTH}${SDAY}.pbs`
qsub -W depend=afterok:$wrfjobid putout_${SYEAR}_${SMONTH}.pbs
