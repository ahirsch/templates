#!/bin/csh
#PBS -q copyq
#PBS -P ${PROJECT}
#PBS -l ncpus=1
#PBS -l mem=3GB
#PBS -l walltime=2:00:00
#PBS -j oe

#
#script to create boundary condition files from files
#in intermediate format using codes provided in WPS.
#

# Module loads
module purge
module load pbs dot
module use /g/data3/hh5/public/modules
module load conda
module switch openmpi/4.0.2
module switch nco/4.7.7
module switch netcdf/4.7.1
module load hdf5/1.8.14
module use ~access/modules
module load nccompress


if (${NOFIRST}) then
   #test to see if the previous wrfbdy file exists - if not then exit
   if ( -e ${BDYDATA_PATH}/wrfbdy_d01_${PYEAR}-${PMONTH} ) then
   else
     echo "${BDYDATA_PATH}/wrfbdy_d01_${PYEAR}-${PMONTH} does not exist"
     exit
   endif
endif

#change to the WPS directory
cd ${WPS_DIR}

#link intermediate files
rm met_em.d0*

foreach base (${BASENAMES})
    rm ${base}*

    set i = 1
    set thismonth = ${smonth}
    set CYEAR = ${SYEAR}
    while ( $i <= ${WRF_nmonths} )
	set CMONTH = `printf "%02d\n" ${thismonth}`
	echo Link files for ${base}:${CYEAR}-${CMONTH}
	ln -s ${DATA_DIR}/${base}:${CYEAR}-${CMONTH}* .
	@ i++
	# Calculate current month and year
	@ thismonth++
	if ( $thismonth > 12 ) then
	   set thismonth = 1
	   @ CYEAR++
	endif
    end
    ln -s ${DATA_DIR}/${base}:${NYEAR}-${NMONTH}-01_00 .
end

echo link intermediate files done

#write the namelist.wps
cat >! ./namelist.wps << EOF_namelist_wps
&share
 wrf_core = 'ARW',
 max_dom = ${max_dom},
 start_date = '${SYEAR}-${SMONTH}-01_00:00:00', '${SYEAR}-${SMONTH}-01_00:00:00', 
 end_date   = '${NYEAR}-${NMONTH}-01_00:00:00', '${NYEAR}-${NMONTH}-01_00:00:00', 
 interval_seconds = 21600,
 io_form_geogrid = 2,
 opt_output_from_geogrid_path = '${GEO_PATH}',
 debug_level = 0,
/

&geogrid
 parent_id         = ${PARENT_ID_AR}
 parent_grid_ratio = ${PARENT_GRID_RATIO_AR}
 i_parent_start    = ${I_PARENT_START_AR}
 j_parent_start    = ${J_PARENT_START_AR}
 e_we          = ${E_WE_AR}
 e_sn          = ${E_SN_AR}
 geog_data_res = 'modis_lakes+10m','2m',
 dx = ${DX},
 dy = ${DY},
 map_proj =  '${gridtype}',
 stand_lon = ${STAND_LON},
 pole_lon  = ${POLE_LON},
 pole_lat  = ${POLE_LAT},
 ref_lat   = ${REF_LAT},
 ref_lon   = ${REF_LON},
 truelat1  = ${TRUELAT1},
 truelat2  = ${TRUELAT2},
 geog_data_path = '/g/data/sx70/data/WPS_GEOG_v3',
/

&ungrib
 out_format = 'WPS',
 prefix = 'NNRPSFC',
/

&metgrid
 onstants_name = 'TAVGSFC'
 fg_name = ${BASENAMES_NAMELIST}
 io_form_metgrid = 2,
 opt_metgrid_tbl_path = '${WPS_DIR}/metgrid'
 opt_output_from_metgrid_path = '${BDYDATA_PATH}'
/

&mod_levs
 press_pa = 201300 , 200100 , 100000 ,
             95000 ,  90000 ,
             85000 ,  80000 ,
             75000 ,  70000 ,
             65000 ,  60000 ,
             55000 ,  50000 ,
             45000 ,  40000 ,
             35000 ,  30000 ,
             25000 ,  20000 ,
             15000 ,  10000 ,
              5000 ,   1000
/
EOF_namelist_wps

# Run avg_tsfc utility for lake initialisation
echo Run avg_tsfc
./util/avg_tsfc.exe

# Run metgrid
echo Run metgrid
./metgrid.exe


#now run real.exe on these metgrid files
cd  ${COUPLED_PATH}

#Clean-up
rm wrfrst_d0*
rm wrfbdy_d0?
rm wrflowinp_d0?
rm wrfout_d0*
rm met_em.d0*

# Link met_em files
ln -s ${BDYDATA_PATH}/met_em.d0* .

# Link LIS output file
if (${FIRST} && ${COUPLED}) then
    echo Link LIS output file
    rm ${SYEAR}${SMONTH}${SDAY}0000.d*.nc
    ncks -d time,-1,-1 ${LISOUT_PATH}/EXP${EXP_NAME}/${LSM}.${PYEAR}${PMONTH}0100.d01.nc ${SYEAR}${SMONTH}${SDAY}0000.d01.nc
# Remove degenerate time dimension
    ncwa -O -a time ${SYEAR}${SMONTH}${SDAY}0000.d01.nc ${SYEAR}${SMONTH}${SDAY}0000.d01.nc
#    ln -s ${LISOUT_PATH}/EXP${EXP_NAME}/${LSM}/${SYEAR}/${SYEAR}${SMONTH}${SDAY}/${SYEAR}${SMONTH}${SDAY}*.d*.nc .
else
    ln -s ${FYEAR}${FMONTH}${FDAY}0000.d01.nc ${SYEAR}${SMONTH}${SDAY}0000.d01.nc
endif

# Remove rsl files
rm rsl.*

#write the namelist.input
cat >! ./namelist.input << EOF_namelist
&time_control
 run_days                            = ${RUNDAYS},
 run_hours                           = 0,
 run_minutes                         = 0,
 run_seconds                         = 0,
 start_year                          = ${SYEAR}, ${SYEAR}, ${SYEAR},
 start_month                         = ${SMONTH},   ${SMONTH},   ${SMONTH},
 start_day                           = ${SDAY},   ${SDAY},   ${SDAY},
 start_hour                          = 00,   00,   00,
 start_minute                        = 00,   00,   00,
 start_second                        = 00,   00,   00,
 end_year                            = ${NYEAR}, ${NYEAR}, ${NYEAR},
 end_month                           = ${NMONTH},   ${NMONTH},   ${NMONTH},
 end_day                             = ${NDAY},   ${NDAY},   ${NDAY},
 end_hour                            = 00,   00,   12,
 end_minute                          = 0,  ${OLAP},  ${OLAP},
 end_second                          = 00,   00,   00,
 interval_seconds                    = 21600
 input_from_file                     = .true.,.true.,.false.,
 history_interval                    = 180, 180,   60,
 frames_per_outfile                  = 1000, 36, 1000,
 restart                             = ${ISRESTART}
 restart_interval                    = ${RES_WRF}
 override_restart_timers             = .true.
 io_form_history                     = 2
 io_form_restart                     = 2
 io_form_input                       = 2
 io_form_boundary                    = 2
 debug_level                         = 0
 auxinput4_inname                    = "wrflowinp_d<domain>"
 auxinput4_interval                  = 360,360,
 io_form_auxinput4                   = 2
 output_diagnostics                  = 1
 auxhist3_outname                    = "wrfxtrm_d<domain>_<date>"
 io_form_auxhist3                    = 2
 auxhist3_interval                   = 1440,1440,1440,
 frames_per_auxhist3                 = 1,1,1,
 auxhist4_outname                    = "wrfhrly_d<domain>_<date>"
 io_form_auxhist4                    = 2
 auxhist4_interval                   = 60,60,60,
 frames_per_auxhist4                 = 24,24,24,
 auxhist7_outname                    = "wrfdly_d<domain>_<date>"
 auxhist7_interval                   = 1440,1440,1440,
 io_form_auxhist7                    = 2,
 frames_per_auxhist7                 = 1,1,1,
 iofields_filename                   = "my_file_d01.txt"
 /

&diagnostics
 max_rain_5m                         = 1,
 max_wind_5m                         = 1,
 max_rain_10m                        = 1,
 max_wind_10m                        = 1,
 max_rain_20m                        = 1,
 max_wind_20m                        = 1,
 max_rain_30m                        = 1,
 max_wind_30m                        = 1,
 max_rain_60m                        = 1,
 max_wind_60m                        = 1,
 max_window                          = 20,
 /

&domains
 time_step                           = ${WRF_ts},
 time_step_fract_num                 = 0,
 time_step_fract_den                 = 1,
 max_dom                             = ${max_dom},
 s_we                                = 1,     1,     1,
 e_we                                = ${E_WE_AR}
 s_sn                                = 1,     1,     1,
 e_sn                                = ${E_SN_AR}
 s_vert                              = 1,     1,     1,
 e_vert                              = 30,    30,    28,
 eta_levels = 1.00,0.995,0.99,0.98,0.97,0.96,0.94,0.92,0.89,0.86,0.83,0.80,0.77,0.72,0.67,0.62,0.57,0.52,0.47,0.42,0.37,0.32,0.27,0.22,0.17,0.12,0.07,0.04,0.02,0.00
 num_metgrid_levels                  = 38
 num_metgrid_soil_levels             = 4
 dx                                  = ${DX_AR}
 dy                                  = ${DY_AR}
 grid_id                             = ${GRID_ID_AR}
 parent_id                           = ${PARENT_ID_AR}
 i_parent_start                      = ${I_PARENT_START_AR}
 j_parent_start                      = ${J_PARENT_START_AR}
 parent_grid_ratio                   = ${PARENT_GRID_RATIO_AR}
 parent_time_step_ratio              = 1,     5,      3,
 feedback                            = 0,
 smooth_option                       = 0
 /

&lis
 lis_landcover_type = 2,
 lis_filename='${SYEAR}${SMONTH}${SDAY}0000.d01.nc'
/

&physics
 mp_physics                          = 4,     4,     3,
 mp_zero_out                         = 2
 mp_zero_out_thresh                  = 1.e-8
 ra_lw_physics                       = 4,     1,     1,
 ra_sw_physics                       = 4,     1,     1,
 radt                                = 10,    10,    10,
 cam_abs_freq_s                      = 10800
 levsiz                              = 59
 paerlev                             = 29
 cam_abs_dim1                        = 4
 cam_abs_dim2                        = 28
 sf_sfclay_physics                   = 2,     1,     1,
 sf_surface_physics                  = 55,     5,     1,
 bl_pbl_physics                      = 2,     1,     1,
 bldt                                = 0,     0,     0,
 cu_physics                          = 16,     1,     0,
 cudt                                = 5,     0,     5,
 isfflx                              = 1,
 ifsnow                              = 0,
 icloud                              = 1,
 surface_input_source                = 1,
 num_soil_layers                     = 6,
 num_land_cat                        = 21,
 sf_urban_physics                    = 0,
 sst_update                          = 1,
 sst_skin                            = 1,
 tmn_update                          = 1,
 lagday                              = 150,
 usemonalb                           = .false.
 slope_rad                           = 1,
 maxiens                             = 1,
 maxens                              = 3,
 maxens2                             = 3,
 maxens3                             = 16,
 ensdim                              = 144,
 bucket_mm                           = 1000000.,
 /

 &fdda
 /

&dynamics
 rk_ord                              = 3,
 w_damping                           = 0,
 diff_opt                            = 1,
 km_opt                              = 4,
 diff_6th_opt                        = 0,
 diff_6th_factor                     = 0.12,
 base_temp                           = 290.
 damp_opt                            = 3,
 zdamp                               = 5000.,  5000.,  5000.,
 dampcoef                            = 0.05,   0.05,   0.01
 khdif                               = 0,      0,      0,
 kvdif                               = 0,      0,      0,
 non_hydrostatic                     = .true., .true., .true.,
 moist_adv_opt                       = 1, 1, 1,
 scalar_adv_opt                      = 1, 1, 1,
 /

&bdy_control
 spec_bdy_width                      = 10,
 spec_zone                           = 1,
 relax_zone                          = 9,
 specified                           = .true., .false.,.false.,
 nested                              = .false., .true., .true.,
 /

 &grib2
 /

 &namelist_quilt
 nio_tasks_per_group = 0,
 nio_groups = 1,
 /
EOF_namelist



echo Run real
./real.exe

echo Start post-processing
if (${WPS_monthly_files}) then
    python ${COUPLED_PATH}/post-process-WPS.py -i ${COUPLED_PATH} -o ${BDYDATA_PATH}
else
    cp wrfbdy_d01 ${BDYDATA_PATH}/wrfbdy_d01_${SYEAR}-${SMONTH}
    # Note: this loop only works for max_dom < 10.
    set ite=1
    while ( $ite <= ${max_dom} )
	cp wrflowinp_d0${ite} ${BDYDATA_PATH}/wrflowinp_d0${ite}_${SYEAR}-${SMONTH}
	if (${NOFIRST} && ${COUPLED}) then
	# If coupled run we only copy the input file for the first month.
	# The other files are not correct.
	else
	    cp wrfinput_d0${ite} ${BDYDATA_PATH}/wrfinput_d0${ite}_${SYEAR}-${SMONTH}
	endif

	@ ite++
    end
endif
# Note: this loop only works for max_dom < 10.
set ite=1
while ( $ite <= ${max_dom} )
   if (${NOFIRST} && ${COUPLED}) then
   # If coupled run we only copy the input file for the first month.
   # The other files are not correct.
   else
      cp wrfinput_d0${ite} ${BDYDATA_PATH}/wrfinput_d0${ite}_${SYEAR}-${SMONTH}
   endif

   @ ite++
end

echo Clean up
rm met_em.d0* ${BDYDATA_PATH}/met_em.d0*

#Compress files
nc_compress -o -pa -m 0 ${BDYDATA_PATH}

#Clean-up
echo Clean up
if (${COUPLED} && ${NOFIRST}) then
    rm ${SYEAR}${SMONTH}${SDAY}*.d*.nc
endif

# Next month
echo Next script
cd ${DECK_PATH}
qsub ./getbdy_${INI_FORMAT}_${NYEAR}_${NMONTH}.deck 
